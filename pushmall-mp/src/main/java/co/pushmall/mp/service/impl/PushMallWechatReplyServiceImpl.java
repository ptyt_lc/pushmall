package co.pushmall.mp.service.impl;

import co.pushmall.exception.EntityExistException;
import co.pushmall.mp.domain.PushMallWechatReply;
import co.pushmall.mp.repository.PushMallWechatReplyRepository;
import co.pushmall.mp.service.PushMallWechatReplyService;
import co.pushmall.mp.service.dto.PushMallWechatReplyDTO;
import co.pushmall.mp.service.dto.PushMallWechatReplyQueryCriteria;
import co.pushmall.mp.service.mapper.PushMallWechatReplyMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-10-10
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallWechatReplyServiceImpl implements PushMallWechatReplyService {

    private final PushMallWechatReplyRepository yxWechatReplyRepository;

    private final PushMallWechatReplyMapper yxWechatReplyMapper;

    public PushMallWechatReplyServiceImpl(PushMallWechatReplyRepository yxWechatReplyRepository, PushMallWechatReplyMapper yxWechatReplyMapper) {
        this.yxWechatReplyRepository = yxWechatReplyRepository;
        this.yxWechatReplyMapper = yxWechatReplyMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallWechatReplyQueryCriteria criteria, Pageable pageable) {
        Page<PushMallWechatReply> page = yxWechatReplyRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(yxWechatReplyMapper::toDto));
    }

    @Override
    public List<PushMallWechatReplyDTO> queryAll(PushMallWechatReplyQueryCriteria criteria) {
        return yxWechatReplyMapper.toDto(yxWechatReplyRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallWechatReplyDTO findById(Integer id) {
        Optional<PushMallWechatReply> yxWechatReply = yxWechatReplyRepository.findById(id);
        ValidationUtil.isNull(yxWechatReply, "PushMallWechatReply", "id", id);
        return yxWechatReplyMapper.toDto(yxWechatReply.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallWechatReplyDTO create(PushMallWechatReply resources) {
        if (yxWechatReplyRepository.findByKey(resources.getKey()) != null) {
            throw new EntityExistException(PushMallWechatReply.class, "key", resources.getKey());
        }
        return yxWechatReplyMapper.toDto(yxWechatReplyRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallWechatReply resources) {
        Optional<PushMallWechatReply> optionalPushMallWechatReply = yxWechatReplyRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallWechatReply, "PushMallWechatReply", "id", resources.getId());
        PushMallWechatReply yxWechatReply = optionalPushMallWechatReply.get();
        PushMallWechatReply yxWechatReply1 = null;
        yxWechatReply1 = yxWechatReplyRepository.findByKey(resources.getKey());
        if (yxWechatReply1 != null && !yxWechatReply1.getId().equals(yxWechatReply.getId())) {
            throw new EntityExistException(PushMallWechatReply.class, "key", resources.getKey());
        }
        yxWechatReply.copy(resources);
        yxWechatReplyRepository.save(yxWechatReply);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        yxWechatReplyRepository.deleteById(id);
    }


    @Override
    public PushMallWechatReply isExist(String key) {
        PushMallWechatReply yxWechatReply = yxWechatReplyRepository.findByKey(key);

        return yxWechatReply;
    }
}
