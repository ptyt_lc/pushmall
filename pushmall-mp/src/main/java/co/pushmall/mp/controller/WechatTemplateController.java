package co.pushmall.mp.controller;

import co.pushmall.mp.domain.PushMallWechatTemplate;
import co.pushmall.mp.service.PushMallWechatTemplateService;
import co.pushmall.mp.service.dto.PushMallWechatTemplateQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-12-10
 */
@Api(tags = "商城:微信模板管理")
@RestController
@RequestMapping("api")
public class WechatTemplateController {

    private final PushMallWechatTemplateService pushMallWechatTemplateService;

    public WechatTemplateController(PushMallWechatTemplateService pushMallWechatTemplateService) {
        this.pushMallWechatTemplateService = pushMallWechatTemplateService;
    }

    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmWechatTemplate")
    @PreAuthorize("@el.check('admin','PushMallWECHATTEMPLATE_ALL','PushMallWECHATTEMPLATE_SELECT')")
    public ResponseEntity getPushMallWechatTemplates(PushMallWechatTemplateQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallWechatTemplateService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @ApiOperation(value = "新增")
    @PostMapping(value = "/PmWechatTemplate")
    @PreAuthorize("@el.check('admin','PushMallWECHATTEMPLATE_ALL','PushMallWECHATTEMPLATE_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallWechatTemplate resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        return new ResponseEntity(pushMallWechatTemplateService.create(resources), HttpStatus.CREATED);
    }

    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmWechatTemplate")
    @PreAuthorize("@el.check('admin','PushMallWECHATTEMPLATE_ALL','PushMallWECHATTEMPLATE_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallWechatTemplate resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallWechatTemplateService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmWechatTemplate/{id}")
    @PreAuthorize("@el.check('admin','PushMallWECHATTEMPLATE_ALL','PushMallWECHATTEMPLATE_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallWechatTemplateService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
