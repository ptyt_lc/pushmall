package co.pushmall.mp.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Entity
@Data
@Table(name = "pushmall_wechat_menu")
public class PushMallWechatMenu implements Serializable {


    @Id
    @Column(name = "`key`")
    private String key;

    // 缓存数据
    @Column(name = "result")
    private String result;

    // 缓存时间
    @Column(name = "add_time")
    private Integer addTime;

    public void copy(PushMallWechatMenu source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
