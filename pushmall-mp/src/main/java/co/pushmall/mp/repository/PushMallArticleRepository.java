package co.pushmall.mp.repository;


import co.pushmall.mp.domain.PushMallArticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-10-07
 */
public interface PushMallArticleRepository extends JpaRepository<PushMallArticle, Integer>, JpaSpecificationExecutor {
}
