package co.pushmall.mp.repository;

import co.pushmall.mp.domain.PushMallWechatTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-12-10
 */
public interface PushMallWechatTemplateRepository extends JpaRepository<PushMallWechatTemplate, Integer>, JpaSpecificationExecutor {
    PushMallWechatTemplate findByTempkey(String key);
}
