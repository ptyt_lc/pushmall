package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreCouponIssue;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-09
 */
//@CacheConfig(cacheNames = "yxStoreCouponIssue")
public interface PushMallStoreCouponIssueService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreCouponIssueQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreCouponIssueDTO> queryAll(PushMallStoreCouponIssueQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreCouponIssueDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreCouponIssueDTO create(PushMallStoreCouponIssue resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreCouponIssue resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
