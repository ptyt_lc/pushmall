package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreDiscount;
import co.pushmall.modules.activity.repository.PushMallStoreDiscountRepository;
import co.pushmall.modules.activity.service.PushMallStoreDiscountService;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreDiscountMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreDiscountServiceImpl implements PushMallStoreDiscountService {

    private final PushMallStoreDiscountRepository PushMallStoreDiscountRepository;

    private final PushMallStoreDiscountMapper PushMallStoreDiscountMapper;

    public PushMallStoreDiscountServiceImpl(PushMallStoreDiscountRepository PushMallStoreDiscountRepository, PushMallStoreDiscountMapper PushMallStoreDiscountMapper) {
        this.PushMallStoreDiscountRepository = PushMallStoreDiscountRepository;
        this.PushMallStoreDiscountMapper = PushMallStoreDiscountMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreDiscountQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreDiscount> page = PushMallStoreDiscountRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(PushMallStoreDiscountMapper::toDto));
    }

    @Override
    public List<PushMallStoreDiscountDTO> queryAll(PushMallStoreDiscountQueryCriteria criteria) {
        return PushMallStoreDiscountMapper.toDto(PushMallStoreDiscountRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreDiscountDTO findById(Integer id) {
        Optional<PushMallStoreDiscount> PushMallStoreDiscount = PushMallStoreDiscountRepository.findById(id);
        ValidationUtil.isNull(PushMallStoreDiscount, "PushMallStoreDiscount", "id", id);
        return PushMallStoreDiscountMapper.toDto(PushMallStoreDiscount.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreDiscountDTO create(PushMallStoreDiscount resources) {
        return PushMallStoreDiscountMapper.toDto(PushMallStoreDiscountRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreDiscount resources) {
        Optional<PushMallStoreDiscount> optionalPushMallStoreDiscount = PushMallStoreDiscountRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreDiscount, "PushMallStoreDiscount", "id", resources.getId());
        PushMallStoreDiscount PushMallStoreDiscount = optionalPushMallStoreDiscount.get();
        PushMallStoreDiscount.copy(resources);
        PushMallStoreDiscountRepository.save(PushMallStoreDiscount);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        PushMallStoreDiscountRepository.deleteById(id);
    }

    @Override
    public PushMallStoreDiscountDTO findByProductId(Integer id) {
        Optional<PushMallStoreDiscount> PushMallStoreDiscount = PushMallStoreDiscountRepository.findByProductId(id);
        if (PushMallStoreDiscount.isPresent()) {
            return PushMallStoreDiscountMapper.toDto(PushMallStoreDiscount.get());
        }
        return new PushMallStoreDiscountDTO();
    }
}
