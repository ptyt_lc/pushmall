package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreVisit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-18
 */
public interface PushMallStoreVisitRepository extends JpaRepository<PushMallStoreVisit, Integer>, JpaSpecificationExecutor {
    int countByProductIdAndProductType(int productId, String productType);
}
