package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallStoreProduct;
import co.pushmall.modules.shop.service.dto.ProductFormatDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductQueryCriteria;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-04
 */
//@CacheConfig(cacheNames = "yxStoreProduct")
public interface PushMallStoreProductService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreProductQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreProductDTO> queryAll(PushMallStoreProductQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreProductDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreProductDTO create(PushMallStoreProduct resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreProduct resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    void recovery(Integer id);

    void onSale(Integer id, Integer status);

    List<ProductFormatDTO> isFormatAttr(Integer id, String jsonStr);

    void createProductAttr(Integer id, String jsonStr);

    void setResult(Map<String, Object> map, Integer id);

    void clearProductAttr(Integer id, boolean isActice);

    String getStoreProductAttrResult(Integer id);

    void download(List<PushMallStoreProductDTO> queryAll, HttpServletResponse response) throws IOException, ParseException;

    int excelToList(InputStream inputStream) throws IOException, InvalidFormatException;

    List<PushMallStoreProductDTO> findByIds(List<String> idList);
}
