package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallStoreOrder;
import co.pushmall.modules.shop.service.dto.PushMallStoreOrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-14
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreOrderMapper extends EntityMapper<PushMallStoreOrderDTO, PushMallStoreOrder> {

}
