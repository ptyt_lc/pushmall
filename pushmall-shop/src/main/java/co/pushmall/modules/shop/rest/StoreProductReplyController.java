package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.domain.PushMallStoreProductReply;
import co.pushmall.modules.shop.service.PushMallStoreProductReplyService;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductReplyQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-03
 */
@Api(tags = "商城:评论管理")
@RestController
@RequestMapping("api")
public class StoreProductReplyController {


    private final PushMallStoreProductReplyService pushMallStoreProductReplyService;

    public StoreProductReplyController(PushMallStoreProductReplyService pushMallStoreProductReplyService) {
        this.pushMallStoreProductReplyService = pushMallStoreProductReplyService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmStoreProductReply")
    @PreAuthorize("@el.check('admin','YXSTOREPRODUCTREPLY_ALL','YXSTOREPRODUCTREPLY_SELECT')")
    public ResponseEntity getPushMallStoreProductReplys(PushMallStoreProductReplyQueryCriteria criteria, Pageable pageable) {
        criteria.setIsDel(0);
        return new ResponseEntity(pushMallStoreProductReplyService.queryAll(criteria, pageable), HttpStatus.OK);
    }


    @Log("修改")
    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmStoreProductReply")
    @PreAuthorize("@el.check('admin','YXSTOREPRODUCTREPLY_ALL','YXSTOREPRODUCTREPLY_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreProductReply resources) {
        pushMallStoreProductReplyService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmStoreProductReply/{id}")
    @PreAuthorize("@el.check('admin','YXSTOREPRODUCTREPLY_ALL','YXSTOREPRODUCTREPLY_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        PushMallStoreProductReply reply = new PushMallStoreProductReply();
        reply.setIsDel(1);
        reply.setId(id);
        pushMallStoreProductReplyService.update(reply);
        return new ResponseEntity(HttpStatus.OK);
    }
}
