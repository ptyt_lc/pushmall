package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallSystemStore;
import co.pushmall.modules.shop.repository.PushMallSystemStoreRepository;
import co.pushmall.modules.shop.service.PushMallSystemStoreService;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreDto;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallSystemStoreMapper;
import co.pushmall.utils.FileUtil;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-03-03
 */
@Service
//@CacheConfig(cacheNames = "yxSystemStore")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallSystemStoreServiceImpl implements PushMallSystemStoreService {

    private final PushMallSystemStoreRepository pushMallSystemStoreRepository;

    private final PushMallSystemStoreMapper pushMallSystemStoreMapper;

    public PushMallSystemStoreServiceImpl(PushMallSystemStoreRepository pushMallSystemStoreRepository, PushMallSystemStoreMapper pushMallSystemStoreMapper) {
        this.pushMallSystemStoreRepository = pushMallSystemStoreRepository;
        this.pushMallSystemStoreMapper = pushMallSystemStoreMapper;
    }

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(PushMallSystemStoreQueryCriteria criteria, Pageable pageable) {
        Page<PushMallSystemStore> page = pushMallSystemStoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallSystemStoreMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<PushMallSystemStoreDto> queryAll(PushMallSystemStoreQueryCriteria criteria) {
        return pushMallSystemStoreMapper.toDto(pushMallSystemStoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public PushMallSystemStoreDto findById(Integer id) {
        PushMallSystemStore pushMallSystemStore = pushMallSystemStoreRepository.findById(id).orElseGet(PushMallSystemStore::new);
        ValidationUtil.isNull(pushMallSystemStore.getId(), "PushMallSystemStore", "id", id);
        return pushMallSystemStoreMapper.toDto(pushMallSystemStore);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public PushMallSystemStoreDto create(PushMallSystemStore resources) {
        return pushMallSystemStoreMapper.toDto(pushMallSystemStoreRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallSystemStore resources) {
        PushMallSystemStore pushMallSystemStore = pushMallSystemStoreRepository.findById(resources.getId()).orElseGet(PushMallSystemStore::new);
        ValidationUtil.isNull(pushMallSystemStore.getId(), "PushMallSystemStore", "id", resources.getId());
        pushMallSystemStore.copy(resources);
        pushMallSystemStoreRepository.save(pushMallSystemStore);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(Integer[] ids) {
        for (Integer id : ids) {
            pushMallSystemStoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<PushMallSystemStoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PushMallSystemStoreDto yxSystemStore : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("门店名称", yxSystemStore.getName());
            map.put("简介", yxSystemStore.getIntroduction());
            map.put("手机号码", yxSystemStore.getPhone());
            map.put("省市区", yxSystemStore.getAddress());
            map.put("详细地址", yxSystemStore.getDetailedAddress());
            map.put("门店logo", yxSystemStore.getImage());
            map.put("纬度", yxSystemStore.getLatitude());
            map.put("经度", yxSystemStore.getLongitude());
            map.put("核销有效日期", yxSystemStore.getValidTime());
            map.put("每日营业开关时间", yxSystemStore.getDayTime());
            map.put("添加时间", yxSystemStore.getAddTime());
            map.put("是否显示", yxSystemStore.getIsShow());
            map.put("是否删除", yxSystemStore.getIsDel());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
