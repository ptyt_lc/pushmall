package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallUser;
import co.pushmall.modules.shop.service.dto.UserMoneyDTO;
import co.pushmall.modules.shop.service.dto.PushMallUserDTO;
import co.pushmall.modules.shop.service.dto.PushMallUserQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-06
 */
//@CacheConfig(cacheNames = "yxUser")
public interface PushMallUserService {

    void updateMoney(UserMoneyDTO param);

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallUserQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallUserDTO> queryAll(PushMallUserQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param uid
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallUserDTO findById(Integer uid);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallUserDTO create(PushMallUser resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallUser resources);

    /**
     * 删除
     *
     * @param uid
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer uid);

    void onStatus(Integer uid, Integer status);

    void onIsPass(Integer uid, Integer isPass, Integer applyLevel);

    void incBrokeragePrice(double price, int uid);

    List<PushMallUserDTO> findByLevel(Integer id);
}
