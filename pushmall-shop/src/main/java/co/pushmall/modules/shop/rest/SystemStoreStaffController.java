package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.exception.BadRequestException;
import co.pushmall.modules.shop.domain.PushMallSystemStoreStaff;
import co.pushmall.modules.shop.service.PushMallSystemStoreStaffService;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffDto;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author pushmall
 * @date 2020-03-22
 */
@Api(tags = "门店店员管理")
@RestController
@RequestMapping("/api/PmSystemStoreStaff")
public class SystemStoreStaffController {

    private final PushMallSystemStoreStaffService pushMallSystemStoreStaffService;

    public SystemStoreStaffController(PushMallSystemStoreStaffService pushMallSystemStoreStaffService) {
        this.pushMallSystemStoreStaffService = pushMallSystemStoreStaffService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('yxSystemStoreStaff:list')")
    public void download(HttpServletResponse response, PushMallSystemStoreStaffQueryCriteria criteria) throws IOException {
        pushMallSystemStoreStaffService.download(pushMallSystemStoreStaffService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询门店店员")
    @ApiOperation("查询门店店员")
    @PreAuthorize("@el.check('yxSystemStoreStaff:list')")
    public ResponseEntity<Object> getPushMallSystemStoreStaffs(PushMallSystemStoreStaffQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity<>(pushMallSystemStoreStaffService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @PostMapping
    @Log("新增门店店员")
    @ApiOperation("新增门店店员")
    @PreAuthorize("@el.check('yxSystemStoreStaff:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PushMallSystemStoreStaff resources) {
        List<PushMallSystemStoreStaffDto> spreadList = pushMallSystemStoreStaffService.findByUid(resources.getUid(), resources.getSpreadUid());
        if (spreadList.size() > 0) {
            throw new BadRequestException("已添加相同店长，请重新选择提交!");
        } else {
            return new ResponseEntity<>(pushMallSystemStoreStaffService.create(resources), HttpStatus.CREATED);
        }
    }

    @PutMapping
    @Log("修改门店店员")
    @ApiOperation("修改门店店员")
    @PreAuthorize("@el.check('yxSystemStoreStaff:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PushMallSystemStoreStaff resources) {
        List<PushMallSystemStoreStaffDto> spreadList = pushMallSystemStoreStaffService.findByUid(resources.getUid(), resources.getSpreadUid());
        if (spreadList.size() > 1) {
            throw new BadRequestException("已添加相同店长，请重新选择提交!");
        } else {
            pushMallSystemStoreStaffService.update(resources);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @Log("删除门店店员")
    @ApiOperation("删除门店店员")
    @PreAuthorize("@el.check('yxSystemStoreStaff:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Integer[] ids) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallSystemStoreStaffService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/find/{uid}/{spreadUid}")
    @Log("查询门店店员")
    @ApiOperation("查询门店店员")
    @PreAuthorize("@el.check('yxSystemStoreStaff:list')")
    public ResponseEntity<Object> find(@PathVariable String uid, @PathVariable String spreadUid) {
        return new ResponseEntity<>(pushMallSystemStoreStaffService.findByUid(Integer.valueOf(uid), Integer.valueOf(spreadUid)), HttpStatus.OK);
    }

}
